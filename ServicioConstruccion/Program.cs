﻿using ServicioConstruccion.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace ServicioConstruccion
{
    class Program
    {
        static void Main(string[] args)
        {


            var exitCode = HostFactory.Run(x =>
            {

                x.Service<ConstruccionServicio>(s =>
                {
                    s.ConstructUsing(h => new ConstruccionServicio());
                    s.WhenStarted(h => h.Start());
                    s.WhenStopped(h => h.Stop());

                });

                x.RunAsLocalSystem();

                x.SetServiceName("ConstrucciónServicio");
                x.SetDisplayName("Servicio Tarifa Agua en contrstrucción******************************************************");
                x.SetDescription("Detecta los predios que tienen agua en construcción y si su fecha de vencimiento se ha agotado crea una orden de trabajo");
            });


            int exitCodeValue = (int)Convert.ChangeType(exitCode, exitCode.GetTypeCode());
            Environment.ExitCode = exitCodeValue;

        }
    }
}
