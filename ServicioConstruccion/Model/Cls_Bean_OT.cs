﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicioConstruccion.Model
{
   public class Cls_Bean_OT
    {
        public string predioId { get; set; }
        public string subFallaId { get; set; } //Puede ser Blanco o DBNull.Value
        public string brigadaId { get; set; } // El ID de la Brigada (se obtiene en la tabla de 
        public string noOrdenTrabajoInicial { get; set; } // DBNull.Value por que tiene referencia a la misma tabla.
        public DateTime fecha { get; set; } // Fecha en la que se crea el documento.
        public string bacheo { get; set; } // Puede ser Blanco o DBNull.Value.
        public string supervisorDistrito { get; set; } // Puede ir en Blanco o DBNull.Value
        public string estatus { get; set; } // IMPRESA
        public string empleadoId { get; set; } // ID del Empleado que crea el documento.
        public string noCuenta { get; set; } // Puede ir en Blanco o DBNull.Value.
        public string brigadaPropuestaId { get; set; }  // Mismo que en el de Brigada_ID.
        public string tipoFallaId { get; set; } // ID del Tipo de Falla.
        public string calleId { get; set; } // ID de la Calle del Predio.
        public string coloniaId { get; set; } // ID de la Colonia del Predio.
        public string calleReferenciaId1 { get; set; } // Si se tienen las referencias de las Calles sino DBNull.Value
        public string calleReferenciaId2 { get; set; } // Si se tienen las referencias de las Calles sino DBNull.Value

        public string numero { get; set; } // Aquí va lo que comentabas del Lote y Manzana.
        public string referencia { get; set; } // Observaciones que tengas para la parte de la Conexión.
        public string reporto { get; set; }  // Va el Nombre de la Cajer@.
        public string domicilioReporte { get; set; } // El Domicilio con Nombres.
        public string telefono { get; set; }// Teléfono
        public string medidor { get; set; }  // Vacío o DBNull.Value
        public string canal { get; set; } // Vacío o DBNull.Value
        public string observaciones { get; set; } // Observaciones que tengas para la parte de la Conexión.
        public string giroId { get; set; } // Giro del Predio.
        public string usuarioCreo { get; set; }
        public string subSecuente { get; set; }  // "NO".
        public string prioridad { get; set; }


        public Cls_Bean_OT()
        {

        }


    }
}
