﻿using ServicioConstruccion.Datos;
using ServicioConstruccion.Model;
using ServicioConstruccion.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Timers;

namespace ServicioConstruccion.Servicio
{
   public class ConstruccionServicio
    {
        private Timer _timer;

        public ConstruccionServicio() {
            // 900000 = 15 minutos     // 600000 = 10 minutos  //  1200000 = 20 minutos // 3600000 = 60 minutos
            _timer = new Timer(3600000)
            { AutoReset = true };
            _timer.Elapsed += TimerElapsed;
        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            DateTime dateTimeHora = DateTime.Now;


            if (dateTimeHora.Hour >= 7 && dateTimeHora.Hour <= 8)
            {
                _timer.Enabled = false;
                Ejecutar_Proceso();
           
            }


        }

        public void Start()
        {
            _timer.Start();
        }


        public void Stop()
        {
            _timer.Stop();
        }

        public void Ejecutar_Proceso()
        {
            DataTable Dt_Predios_Construccion;
            Cls_Construccion_Datos cls_Construccion_Datos = new Cls_Construccion_Datos();
            int contador;

            SqlConnection ObjConexion = null;
            SqlTransaction ObjTransaccion = null;

            Cls_Bean_OT cls_Bean_OT;

            try
            {
          

                Dt_Predios_Construccion = cls_Construccion_Datos.Obtener_Predios_Fecha_Construcion();

                if (Dt_Predios_Construccion.Rows.Count > 0)
                {

                    ObjConexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                    ObjConexion.Open();

                    ObjTransaccion = ObjConexion.BeginTransaction();

                    for (contador =0; contador < Dt_Predios_Construccion.Rows.Count; contador++)
                    {

                        cls_Bean_OT = new Cls_Bean_OT();

                        cls_Bean_OT.predioId = Dt_Predios_Construccion.Rows[0]["Predio_ID"].ToString();
                        cls_Bean_OT.brigadaId = Dt_Predios_Construccion.Rows[0]["Brigada_ID"].ToString();
                        cls_Bean_OT.fecha = DateTime.Now;
                        cls_Bean_OT.estatus = "ASIGNADA";
                        cls_Bean_OT.empleadoId = Dt_Predios_Construccion.Rows[0]["Empleado_ID"].ToString();
                        cls_Bean_OT.noCuenta = "";
                        cls_Bean_OT.brigadaPropuestaId = Dt_Predios_Construccion.Rows[0]["Brigada_ID"].ToString();
                        cls_Bean_OT.tipoFallaId = Dt_Predios_Construccion.Rows[0]["Falla_ID"].ToString();
                        cls_Bean_OT.calleId = Dt_Predios_Construccion.Rows[0]["Calle_ID"].ToString();
                        cls_Bean_OT.coloniaId = Dt_Predios_Construccion.Rows[0]["Colonia_ID"].ToString();
                        cls_Bean_OT.prioridad = "NORMAL";

                        if (Dt_Predios_Construccion.Rows[0]["Calle_Referencia1_ID"].ToString().Trim().Length > 0)
                            cls_Bean_OT.calleReferenciaId1 = Dt_Predios_Construccion.Rows[0]["Calle_Referencia1_ID"].ToString();

                        if (Dt_Predios_Construccion.Rows[0]["Calle_Referencia2_ID"].ToString().Trim().Length > 0)
                            cls_Bean_OT.calleReferenciaId1 = Dt_Predios_Construccion.Rows[0]["Calle_Referencia2_ID"].ToString();

                        cls_Bean_OT.numero = "No. Ext: " + Dt_Predios_Construccion.Rows[0]["Numero_Exterior"].ToString() + " " + Dt_Predios_Construccion.Rows[0]["Numero_Interior"].ToString();
                        cls_Bean_OT.numero += "Mz: " + Dt_Predios_Construccion.Rows[0]["Manzana"].ToString() + " Lot: " + Dt_Predios_Construccion.Rows[0]["Lote"].ToString();
                        cls_Bean_OT.referencia = "";
                        cls_Bean_OT.reporto = "servicio construccion";
                        cls_Bean_OT.domicilioReporte = Dt_Predios_Construccion.Rows[0]["Colonia"].ToString() + "  " + Dt_Predios_Construccion.Rows[0]["Calle"].ToString();

                        if (Dt_Predios_Construccion.Rows[0]["Telefono_Casa"].ToString().Length > 0)
                            cls_Bean_OT.telefono = "Tel. casa: " + Dt_Predios_Construccion.Rows[0]["Telefono_Casa"].ToString();
                        if (Dt_Predios_Construccion.Rows[0]["Telefono_Celular"].ToString().Length > 0)
                            cls_Bean_OT.telefono += "Tel. cel: " + Dt_Predios_Construccion.Rows[0]["Telefono_Celular"].ToString();

                        cls_Bean_OT.telefono = cls_Bean_OT.telefono.Trim();

                        cls_Bean_OT.observaciones = "Orden Generada Automáticamente";
                        cls_Bean_OT.giroId = Dt_Predios_Construccion.Rows[0]["Giro_ID"].ToString();
                        cls_Bean_OT.usuarioCreo = "servicio construccion";
                        cls_Bean_OT.subSecuente = "NO";


                        cls_Construccion_Datos.Registrar_Orden_Trabajo(ObjConexion, ObjTransaccion, cls_Bean_OT);
                        cls_Construccion_Datos.Aplicar_Bandera_Orden_Construccion(ObjConexion, ObjTransaccion, cls_Bean_OT.predioId);
                    }

                    ObjTransaccion.Commit();

                }


            }
            catch (Exception ex)
            {
                Utility.Logger(ex.Message);

                if (ObjTransaccion != null)
                {
                    ObjTransaccion.Rollback();
                }

            }
            finally {
                _timer.Enabled = true;

                if(ObjConexion != null)
                {
                    ObjConexion.Close();
                    ObjConexion = null;
                }
            }

        }
    }
}
