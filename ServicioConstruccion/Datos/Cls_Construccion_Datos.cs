﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using ServicioConstruccion.Util;
using ServicioConstruccion.Model;

namespace ServicioConstruccion.Datos
{
    public class Cls_Construccion_Datos
    {
        public string str_sql;


        public DataTable Obtener_Predios_Fecha_Construcion() {
            DataTable dt = new DataTable();
            SqlDataReader dr;
            using (SqlConnection conexion = new SqlConnection(Cls_Constantes.Str_Conexion))
            {
                conexion.Open();

                using (SqlCommand comando = conexion.CreateCommand())
                {
                    str_sql = "sp_obtener_predios_en_construccion";
                    comando.CommandText = str_sql;
                    comando.CommandType = CommandType.StoredProcedure;
                    dr = comando.ExecuteReader();

                    dt.Load(dr);
                }
            }

            return dt;

        }


        public void Aplicar_Bandera_Orden_Construccion(SqlConnection conexion, SqlTransaction transaccion, string Predio_ID) {

            using (SqlCommand comando = conexion.CreateCommand()) {

                comando.Transaction = transaccion;
                comando.CommandText = "sp_aplicado_orden_construccion";
                comando.CommandType = CommandType.StoredProcedure;

                comando.Parameters.Add(new SqlParameter("@predio_id", Predio_ID));

                comando.ExecuteNonQuery();
            }

        }

        public string Registrar_Orden_Trabajo(SqlConnection conexion, SqlTransaction transaccion, Cls_Bean_OT ObjBeanOT) {
            string respuesta = "";


            using (SqlCommand comando = conexion.CreateCommand())
            {
                comando.Transaction = transaccion;


                comando.CommandText = "SP_Insertar_Orden_Trabajo";
                comando.CommandType = CommandType.StoredProcedure;

                comando.Parameters.Add(new SqlParameter("@Predio_ID", ObjBeanOT.predioId));

                if (ObjBeanOT.subFallaId != null)
                    comando.Parameters.Add(new SqlParameter("@Sub_Falla_ID", ObjBeanOT.subFallaId));
                else
                    comando.Parameters.Add(new SqlParameter("@Sub_Falla_ID", DBNull.Value));

                comando.Parameters.Add(new SqlParameter("@Brigada_ID", ObjBeanOT.brigadaId));


                if (ObjBeanOT.noOrdenTrabajoInicial != null)
                    comando.Parameters.Add(new SqlParameter("@No_Orden_Trabajo_Inicial", ObjBeanOT.noOrdenTrabajoInicial));
                else
                    comando.Parameters.Add(new SqlParameter("@No_Orden_Trabajo_Inicial", DBNull.Value));

                comando.Parameters.Add(new SqlParameter("@Fecha", ObjBeanOT.fecha));

                if (ObjBeanOT.bacheo != null)
                    comando.Parameters.Add(new SqlParameter("@Bacheo", ObjBeanOT.bacheo));
                else
                    comando.Parameters.Add(new SqlParameter("@Bacheo", DBNull.Value));

                if (ObjBeanOT.supervisorDistrito != null)
                    comando.Parameters.Add(new SqlParameter("@Supervisor_Distrito", ObjBeanOT.supervisorDistrito));
                else
                    comando.Parameters.Add(new SqlParameter("@Supervisor_Distrito", DBNull.Value));


                comando.Parameters.Add(new SqlParameter("@Estatus", ObjBeanOT.estatus));
                comando.Parameters.Add(new SqlParameter("@Empleado_ID", ObjBeanOT.empleadoId));
                comando.Parameters.Add(new SqlParameter("@No_Cuenta", ObjBeanOT.noCuenta));

                comando.Parameters.Add(new SqlParameter("@Brigada_Propuesta_ID", ObjBeanOT.brigadaId));
                comando.Parameters.Add(new SqlParameter("@Tipo_Falla_ID", ObjBeanOT.tipoFallaId));
                comando.Parameters.Add(new SqlParameter("@Calle_ID", ObjBeanOT.calleId));
                comando.Parameters.Add(new SqlParameter("@Colonia_ID", ObjBeanOT.coloniaId));


                if (ObjBeanOT.calleReferenciaId1 != null)
                    comando.Parameters.Add(new SqlParameter("@CALLE_REFERENCIA1_ID", ObjBeanOT.calleReferenciaId1));
                else
                    comando.Parameters.Add(new SqlParameter("@CALLE_REFERENCIA1_ID", DBNull.Value));

                if (ObjBeanOT.calleReferenciaId2 != null)
                    comando.Parameters.Add(new SqlParameter("@CALLE_REFERENCIA2_ID", ObjBeanOT.calleReferenciaId2));
                else
                    comando.Parameters.Add(new SqlParameter("@CALLE_REFERENCIA2_ID", DBNull.Value));

                comando.Parameters.Add(new SqlParameter("@Numero", ObjBeanOT.numero));
                comando.Parameters.Add(new SqlParameter("@Referencia", ObjBeanOT.referencia));
                comando.Parameters.Add(new SqlParameter("@Reporto", ObjBeanOT.reporto));
                comando.Parameters.Add(new SqlParameter("@Domicilio_Reporto", ObjBeanOT.domicilioReporte));
                comando.Parameters.Add(new SqlParameter("@Telefono", ObjBeanOT.telefono));


                if (ObjBeanOT.medidor != null)
                    comando.Parameters.Add(new SqlParameter("@Medidor", ObjBeanOT.medidor));
                else
                    comando.Parameters.Add(new SqlParameter("@Medidor", DBNull.Value));

                if (ObjBeanOT.canal != null)
                    comando.Parameters.Add(new SqlParameter("@Canal", ObjBeanOT.canal));
                else
                    comando.Parameters.Add(new SqlParameter("@Canal", DBNull.Value));


                comando.Parameters.Add(new SqlParameter("@Observaciones", ObjBeanOT.observaciones));
                comando.Parameters.Add(new SqlParameter("@Giro_ID", ObjBeanOT.giroId));
                comando.Parameters.Add(new SqlParameter("@USUARIO_CREO", ObjBeanOT.usuarioCreo));
                comando.Parameters.Add(new SqlParameter("@Subsecuente", ObjBeanOT.subSecuente));
                comando.Parameters.Add(new SqlParameter("@Prioridad", ObjBeanOT.prioridad));

                respuesta = comando.ExecuteScalar().ToString();

            }




            return respuesta;

        }
    }
}
